#include "pch.h"
#include "GameManager.h"
#include "GameObject.h"
#include "GameCollider.h"
#include "GameObject.h"
#include "Score.h"
#include "Brick.h"
#include "GameField.h"
#include "Ball.h"

GameManager::GameManager() : m_numBlocks(0) {}

bool GameManager::canCollide(CollisionTag i_tag1, CollisionTag i_tag2)
{
	return m_collisionMatrix[i_tag1][i_tag2];
}

GameManager& GameManager::instance()
{
	static GameManager INSTANCE;
	return INSTANCE;
}

void GameManager::addGameObject(GameObject* i_gameObjects)
{
	m_gameObjects.push_back(i_gameObjects);
}

std::vector<GameObject*> GameManager::getGameObjects()
{
	return m_gameObjects;
}

std::vector<GameCollider*> GameManager::getGameColliders(CollisionTag i_tag)
{
	std::vector<GameCollider*> colliders = std::vector<GameCollider*>();
	for (GameObject* gameObject : m_gameObjects) {
		for (GameCollider* collider : gameObject->getColliders()) {
			if (canCollide(i_tag, collider->getCollisionTag())) {
				colliders.push_back(collider);
			}
		}
	}
	return colliders;
}

void GameManager::setNumBlocs(int i_numBlocks)
{
	m_numBlocks = i_numBlocks;
}

int GameManager::getNumBlocs()
{
	return m_numBlocks;
}

void GameManager::incrementNumBlocs()
{
	m_numBlocks++;
}

void GameManager::decrementNumBlocs()
{
	m_numBlocks--;
}

void GameManager::gameOver(bool i_restart)
{
	for (GameObject* gameObject : m_gameObjects) {
		if (gameObject != nullptr) {
			delete gameObject;
			gameObject = nullptr;
		}
	}
	m_gameObjects.clear();
	setNumBlocs(0);

	if (i_restart) {

		DirectX::SimpleMath::Vector2 bricksCenter = m_center;
		bricksCenter += DirectX::SimpleMath::Vector2(0.0f, -200.0f);

		// create field
		addGameObject(new GameField(m_center, DirectX::SimpleMath::Vector2(780.0f, 580.0f)));

		float ySpace = 40.0f;

		// create 24 bricks
		for (int row = 0; row < 3; row++) {
			for (int column = 0; column < 8; column++) {
				addGameObject(new Brick(bricksCenter + DirectX::SimpleMath::Vector2(-280.0f + column * 80.0f, 0.0f + row * 30.0f + ySpace * row), (row % 2 == 0 ? 1 : 2)));
				incrementNumBlocs();
			}
		}

		// create score
		addGameObject(new Score(m_center + DirectX::SimpleMath::Vector2(0.0f, 220.0f)));

		// create ball
		addGameObject(new Ball(m_center + DirectX::SimpleMath::Vector2(0.0f, 180.0f)));
	}
}

void GameManager::setCenter(DirectX::SimpleMath::Vector2 i_center)
{
	m_center = i_center;
}
