#include "pch.h"
#include "GameObjectBase.h"

GameObjectBase::GameObjectBase() :
	GameObjectBase(DirectX::SimpleMath::Vector2(0.0f, 0.0f), DirectX::SimpleMath::Vector2(0.0f, 0.0f)) {}

GameObjectBase::GameObjectBase(DirectX::SimpleMath::Vector2 i_position) :
	GameObjectBase(i_position, DirectX::SimpleMath::Vector2(0.0f, 0.0f)) {}

GameObjectBase::GameObjectBase(DirectX::SimpleMath::Vector2 i_position, DirectX::SimpleMath::Vector2 i_size) :
	m_position(i_position), m_halfSize(i_size / 2.0f) {}

void GameObjectBase::Update(float deltaTime, const DirectX::Keyboard::State& i_kb) {}

void GameObjectBase::Draw(DirectX::PrimitiveBatch<VertexType>* batch) const {}

void GameObjectBase::setPosition(DirectX::SimpleMath::Vector2 i_position)
{
	m_position = i_position;
}

DirectX::SimpleMath::Vector2 GameObjectBase::getPosition()
{
	return m_position;
}

DirectX::SimpleMath::Vector2 GameObjectBase::getSize()
{
	return m_halfSize * 2.0f;
}


DirectX::SimpleMath::Vector2 GameObjectBase::getHalfSize()
{
	return m_halfSize;
}