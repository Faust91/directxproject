#pragma once
#include "GameObject.h"
class GameField : public GameObject
{
public:
	GameField(DirectX::SimpleMath::Vector2 i_position, DirectX::SimpleMath::Vector2 i_size);
	virtual void Update(float i_deltaTime, const DirectX::Keyboard::State& i_kb) override;
	virtual void Draw(DirectX::PrimitiveBatch<GameObject::VertexType>* i_batch) const override;
};

