#include "pch.h"
#include "Ball.h"
#include "GameCollider.h"
#include <random>

namespace {
	constexpr float BALL_SPEED = 400.0f;
}

Ball::Ball(DirectX::SimpleMath::Vector2 i_position) : GameObject(i_position, DirectX::SimpleMath::Vector2(15.0f, 15.0f), DirectX::Colors::Yellow)
{

	m_colliders.push_back(new GameCollider(this, GameManager::CollisionTag::ball, DirectX::SimpleMath::Vector2(0.0f, 0.0f), m_halfSize * 2));

	std::random_device dev;
	std::mt19937 rng(dev());
	std::uniform_int_distribution<std::mt19937::result_type> dist(0, 90);
	float randomAngle = (float)(dist(rng) - 45) * 3.141592653589793f / 180.0f;

	setVelocity(DirectX::SimpleMath::Vector2::Transform(DirectX::SimpleMath::Vector2(0.0f, -1.0f),
		DirectX::SimpleMath::Matrix::CreateRotationZ(randomAngle)) * BALL_SPEED);
}

void Ball::Update(float i_deltaTime, const DirectX::Keyboard::State& i_kb)
{
	yReverted = false;
	xReverted = false;
	GameObject::Update(i_deltaTime, i_kb);
}

void Ball::Draw(DirectX::PrimitiveBatch<GameObject::VertexType>* i_batch) const
{
	GameObject::Draw(i_batch);
}

void Ball::OnCollisionBegin(GameCollider* i_me, GameCollider* i_other)
{
	GameObject::OnCollisionBegin(i_me, i_other);

	if (i_other->getCollisionTag() == GameManager::CollisionTag::death) {
		// game over (i lose)
		setVelocity(DirectX::SimpleMath::Vector2(0.0f, 0.0f));
		GameManager::instance().gameOver(false);
	}
	else {

		bool revertX = false, revertY = false;

		DirectX::SimpleMath::Rectangle intersection = DirectX::SimpleMath::Rectangle::Intersect(i_me->getWorldRectangle(), i_other->getWorldRectangle());

		if (intersection.width >= intersection.height && !yReverted) {
			revertY = true;
			yReverted = true;
		}
		else if (!xReverted) {
			revertX = true;
			xReverted = true;
		}

		//DirectX::SimpleMath::Rectangle otherRect = i_other->getWorldRectangle();

		//revertX = (this->getPosition().x < otherRect.x || this->getPosition().x > otherRect.x + otherRect.width);
		//revertY = (this->getPosition().y < otherRect.y || this->getPosition().y > otherRect.y + otherRect.height);

		//DirectX::SimpleMath::Rectangle::Intersect(i_me->getWorldRectangle(), i_other->getWorldRectangle());

		if (revertX) {
			// revert x velocity
			setVelocity(DirectX::SimpleMath::Vector2(-getVelocity().x, getVelocity().y));
		}

		if (revertY) {
			// revert y velocity
			setVelocity(DirectX::SimpleMath::Vector2(getVelocity().x, -getVelocity().y));
		}

		if (i_other->getCollisionTag() == GameManager::CollisionTag::score) {
			float devAngle = (m_position.x - i_other->getParent()->getPosition().x) / i_other->getParent()->getHalfSize().x;
			devAngle *= 75.0f;
			devAngle = devAngle * 3.141592653589793f / 180.0f;
			setVelocity(DirectX::SimpleMath::Vector2::Transform(DirectX::SimpleMath::Vector2(0.0f, -1.0f),
				DirectX::SimpleMath::Matrix::CreateRotationZ(devAngle)) * BALL_SPEED);

			/*DirectX::SimpleMath::Vector2 scoreVelocity = i_other->getParent()->getVelocity();
			if (scoreVelocity.x < -MIN_VELOCITY || scoreVelocity.x > MIN_VELOCITY) {
				m_velocity += scoreVelocity / 10.0f;
				m_velocity.Normalize();
				m_velocity *= BALL_SPEED;
			}*/
		}
	}
}

void Ball::OnCollisionEnd(GameCollider* i_me, GameCollider* i_other)
{
	if (GameManager::instance().getNumBlocs() <= 0) {
		// game over (i win)
		setVelocity(DirectX::SimpleMath::Vector2(0.0f, 0.0f));
		GameManager::instance().gameOver(true);
	}
}
