#pragma once

#include "GameObjectBase.h"

class GameCollider;

class GameObject : public GameObjectBase
{
protected:
	static constexpr float MIN_VELOCITY = 0.001f;

	DirectX::SimpleMath::Vector2 m_velocity;
	float m_padding;
	DirectX::XMVECTORF32 m_color;
	DirectX::XMVECTORF32 m_paddingColor;
	std::vector<GameCollider*> m_colliders;

public:
	GameObject();
	GameObject(DirectX::SimpleMath::Vector2 i_position);
	GameObject(DirectX::SimpleMath::Vector2 i_position, DirectX::SimpleMath::Vector2 i_size);
	GameObject(DirectX::SimpleMath::Vector2 i_position, DirectX::SimpleMath::Vector2 i_size, DirectX::XMVECTORF32 i_color);
	GameObject(DirectX::SimpleMath::Vector2 i_position, DirectX::SimpleMath::Vector2 i_size, float i_padding, DirectX::XMVECTORF32 i_color, DirectX::XMVECTORF32 i_paddingColor);
	virtual void Update(float i_deltaTime, const DirectX::Keyboard::State& i_kb) override;
	virtual void Draw(DirectX::PrimitiveBatch<VertexType>* i_batch) const override;
	void setVelocity(DirectX::SimpleMath::Vector2 i_velocity);
	DirectX::SimpleMath::Vector2 getVelocity();
	virtual void OnCollisionBegin(GameCollider* i_me, GameCollider* i_other);
	virtual void OnCollisionStay(GameCollider* i_me, GameCollider* i_other);
	virtual void OnCollisionEnd(GameCollider* i_me, GameCollider* i_other);
	std::vector<GameCollider*> getColliders();
	virtual ~GameObject();
};

