#pragma once
#include "GameObject.h"
class Score : public GameObject
{
public:
	Score(DirectX::SimpleMath::Vector2 i_position);
	virtual void Update(float i_deltaTime, const DirectX::Keyboard::State& i_kb) override;
	virtual void Draw(DirectX::PrimitiveBatch<GameObject::VertexType>* i_batch) const override;
	virtual void OnCollisionBegin(GameCollider* i_me, GameCollider* i_other) override;
	virtual void OnCollisionStay(GameCollider* i_me, GameCollider* i_other) override;

};

