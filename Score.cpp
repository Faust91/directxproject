#include "pch.h"
#include "Score.h"
#include "GameManager.h"
#include "GameCollider.h"
#include <iostream>

namespace {
	constexpr float SCORE_SPEED = 500.0f;
}

Score::Score(DirectX::SimpleMath::Vector2 i_position) : GameObject(i_position, DirectX::SimpleMath::Vector2(100.0f, 25.0f), DirectX::Colors::White)
{
	m_colliders.push_back(new GameCollider(this, GameManager::CollisionTag::score, DirectX::SimpleMath::Vector2(0.0f, 0.0f), m_halfSize * 2));
}

void Score::Update(float i_deltaTime, const DirectX::Keyboard::State& i_kb)
{
	GameObject::Update(i_deltaTime, i_kb);
	DirectX::SimpleMath::Vector2 move = DirectX::SimpleMath::Vector2::Zero;

	if (i_kb.Left || i_kb.A)
		move.x -= 1.f;

	if (i_kb.Right || i_kb.D)
		move.x += 1.f;

	move *= SCORE_SPEED;

	setVelocity(move);

	//setPosition(getPosition() + move);
}

void Score::Draw(DirectX::PrimitiveBatch<GameObject::VertexType>* i_batch) const
{
	GameObject::Draw(i_batch);
}

void Score::OnCollisionBegin(GameCollider* i_me, GameCollider* i_other)
{
	if (i_other->getCollisionTag() == GameManager::CollisionTag::wall) {
		DirectX::SimpleMath::Rectangle wallRect = i_other->getWorldRectangle();
		if (m_position.x > wallRect.Center().x) {
			setPosition(DirectX::SimpleMath::Vector2(wallRect.x + wallRect.width + m_halfSize.x, m_position.y));
		}
		else {
			setPosition(DirectX::SimpleMath::Vector2(wallRect.x - m_halfSize.x, m_position.y));
		}
	}
}

void Score::OnCollisionStay(GameCollider* i_me, GameCollider* i_other)
{
	if (i_other->getCollisionTag() == GameManager::CollisionTag::wall) {
		DirectX::SimpleMath::Rectangle wallRect = i_other->getWorldRectangle();
		if (m_position.x > wallRect.Center().x) {
			setPosition(DirectX::SimpleMath::Vector2(wallRect.x + wallRect.width + m_halfSize.x, m_position.y));
		}
		else {
			setPosition(DirectX::SimpleMath::Vector2(wallRect.x - m_halfSize.x, m_position.y));
		}
	}
}
