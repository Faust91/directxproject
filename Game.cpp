//
// Game.cpp
//

#include "pch.h"
#include "Game.h"
#include "GameObject.h"
#include "GameManager.h"

extern void ExitGame() noexcept;

using namespace DirectX;
using namespace DirectX::SimpleMath;

using Microsoft::WRL::ComPtr;

//namespace
//{
//	const XMVECTORF32 START_POSITION = { 0.f, -1.5f, 0.f, 0.f };
//	const XMVECTORF32 ROOM_BOUNDS = { 8.f, 6.f, 12.f, 0.f };
//	constexpr float ROTATION_GAIN = 0.004f;
//	constexpr float MOVEMENT_GAIN = 0.07f;
//}

//namespace
//{
//	//const XMVECTORF32 ROOM_BOUNDS = { 8.f, 6.f, 12.f, 0.f };
//	//constexpr float ROTATION_GAIN = 0.004f;
//	//constexpr float MOVEMENT_GAIN = 0.07f;
//
//	//constexpr float c_defaultPhi = XM_2PI / 6.0f;
//	//constexpr float c_defaultRadius = 3.3f;
//	//constexpr float c_minRadius = 0.1f;
//	//constexpr float c_maxRadius = 5.f;
//
//	//constexpr float SCORE_SPEED = 5.0f;
//}


Game::Game() noexcept(false)
//:
//m_theta(0.f),
//m_phi(c_defaultPhi),
//m_radius(c_defaultRadius),
//m_roomColor(Colors::White)
{
	m_deviceResources = std::make_unique<DX::DeviceResources>();
	m_deviceResources->RegisterDeviceNotify(this);
}

// Initialize the Direct3D resources required to run.
void Game::Initialize(HWND window, int width, int height)
{
	m_deviceResources->SetWindow(window, width, height);

	m_deviceResources->CreateDeviceResources();
	CreateDeviceDependentResources();

	m_deviceResources->CreateWindowSizeDependentResources();
	CreateWindowSizeDependentResources();

	// TODO: Change the timer settings if you want something other than the default variable timestep mode.
	// e.g. for 60 FPS fixed timestep update logic, call:
	/*
	m_timer.SetFixedTimeStep(true);
	m_timer.SetTargetElapsedSeconds(1.0 / 60);
	*/

	m_keyboard = std::make_unique<Keyboard>();
	m_mouse = std::make_unique<Mouse>();
	m_mouse->SetWindow(window);

	GameManager::instance().setCenter(m_center);
	GameManager::instance().gameOver(true);
}

#pragma region Frame Update
// Executes the basic game loop.
void Game::Tick()
{
	m_timer.Tick([&]()
		{
			Update(m_timer);
		});

	Render();
}

// Updates the world.
void Game::Update(DX::StepTimer const& timer)
{
	float elapsedTime = float(timer.GetElapsedSeconds());

	// TODO: Add your game logic here.
	elapsedTime;

	//auto mouse = m_mouse->GetState();
	//m_mouseButtons.Update(mouse);

	//m_radius -= float(mouse.scrollWheelValue) * ROTATION_GAIN;
	//m_mouse->ResetScrollWheelValue();
	//m_radius = std::max(c_minRadius, std::min(c_maxRadius, m_radius));

	//if (mouse.positionMode == Mouse::MODE_RELATIVE)
	//{
	//	/*Vector3 delta = Vector3(float(mouse.x), float(mouse.y), 0.f)
	//		* ROTATION_GAIN;*/

	//		//m_phi -= delta.y;
	//		//m_theta -= delta.x;
	//}

	//m_mouse->SetMode(mouse.leftButton ?
	//	Mouse::MODE_RELATIVE : Mouse::MODE_ABSOLUTE);

	auto kb = m_keyboard->GetState();
	m_keys.Update(kb);

	if (kb.Escape)
	{
		ExitGame();
	}

	for (GameObject* gameObject : GameManager::instance().getGameObjects()) {
		gameObject->Update(elapsedTime, kb);
	}

}
#pragma endregion

#pragma region Frame Render
// Draws the scene.
void Game::Render()
{
	// Don't try to render anything before the first Update.
	if (m_timer.GetFrameCount() == 0)
	{
		return;
	}

	Clear();

	m_deviceResources->PIXBeginEvent(L"Render");
	auto context = m_deviceResources->GetD3DDeviceContext();

	// TODO: Add your rendering code here.
	context;

	context->OMSetBlendState(m_states->Opaque(), nullptr, 0xFFFFFFFF);
	context->OMSetDepthStencilState(m_states->DepthNone(), 0);
	context->RSSetState(m_states->CullNone());

	m_effect->Apply(context);

	context->IASetInputLayout(m_inputLayout.Get());

	m_batch->Begin();

	for (const GameObject* gameObject : GameManager::instance().getGameObjects()) {
		gameObject->Draw(m_batch.get());
	}

	m_batch->End();



	m_deviceResources->PIXEndEvent();

	// Show the new frame.
	m_deviceResources->Present();
	m_graphicsMemory->Commit();
}

// Helper method to clear the back buffers.
void Game::Clear()
{
	m_deviceResources->PIXBeginEvent(L"Clear");

	// Clear the views.
	auto context = m_deviceResources->GetD3DDeviceContext();
	auto renderTarget = m_deviceResources->GetRenderTargetView();
	auto depthStencil = m_deviceResources->GetDepthStencilView();

	context->ClearRenderTargetView(renderTarget, Colors::Black);
	context->ClearDepthStencilView(depthStencil, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	context->OMSetRenderTargets(1, &renderTarget, depthStencil);

	// Set the viewport.
	auto viewport = m_deviceResources->GetScreenViewport();
	context->RSSetViewports(1, &viewport);

	m_deviceResources->PIXEndEvent();
}
#pragma endregion

#pragma region Message Handlers
// Message handlers
void Game::OnActivated()
{
	// TODO: Game is becoming active window.
	m_keys.Reset();
	m_mouseButtons.Reset();

}

void Game::OnDeactivated()
{
	// TODO: Game is becoming background window.
}

void Game::OnSuspending()
{
	// TODO: Game is being power-suspended (or minimized).
}

void Game::OnResuming()
{
	m_timer.ResetElapsedTime();

	// TODO: Game is being power-resumed (or returning from minimize).
	m_keys.Reset();
	m_mouseButtons.Reset();

}

void Game::OnWindowMoved()
{
	auto r = m_deviceResources->GetOutputSize();
	m_deviceResources->WindowSizeChanged(r.right, r.bottom);
}

void Game::OnWindowSizeChanged(int width, int height)
{
	if (!m_deviceResources->WindowSizeChanged(width, height))
		return;

	CreateWindowSizeDependentResources();

	// TODO: Game window is being resized.
}

// Properties
void Game::GetDefaultSize(int& width, int& height) const noexcept
{
	// TODO: Change to desired default window size (note minimum size is 320x200).
	width = 800;
	height = 600;
}
#pragma endregion

#pragma region Direct3D Resources
// These are the resources that depend on the device.
void Game::CreateDeviceDependentResources()
{
	auto device = m_deviceResources->GetD3DDevice();
	auto context = m_deviceResources->GetD3DDeviceContext();

	// TODO: Initialize device dependent objects here (independent of window size).
	m_graphicsMemory = std::make_unique<GraphicsMemory>(device);
	m_states = std::make_unique<CommonStates>(device);
	m_effect = std::make_unique<BasicEffect>(device);
	m_batch = std::make_unique<PrimitiveBatch<VertexType>>(context);

	m_effect->SetVertexColorEnabled(true);

	DX::ThrowIfFailed(
		CreateInputLayoutFromEffect<VertexType>(device, m_effect.get(),
			m_inputLayout.ReleaseAndGetAddressOf())
	);


}

// Allocate all memory resources that change on a window SizeChanged event.
void Game::CreateWindowSizeDependentResources()
{
	// TODO: Initialize windows-size dependent objects here.
	auto size = m_deviceResources->GetOutputSize();

	m_center.x = float(size.right) / 2.0f;
	m_center.y = float(size.bottom) / 2.0f;

	Matrix proj = Matrix::CreateScale(2.f / float(size.right),
		-2.f / float(size.bottom), 1.f)
		* Matrix::CreateTranslation(-1.f, 1.f, 0.f);

	m_effect->SetProjection(proj);

}

void Game::OnDeviceLost()
{
	// TODO: Add Direct3D resource cleanup here.
	m_graphicsMemory.reset();
	m_states.reset();
	m_effect.reset();
	m_batch.reset();
	m_inputLayout.Reset();
}

void Game::OnDeviceRestored()
{
	CreateDeviceDependentResources();

	CreateWindowSizeDependentResources();
}
#pragma endregion
