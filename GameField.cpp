#include "pch.h"
#include "GameField.h"
#include "GameManager.h"
#include "GameCollider.h"

GameField::GameField(DirectX::SimpleMath::Vector2 i_position, DirectX::SimpleMath::Vector2 i_size) : GameObject(i_position, i_size, 2.0f, DirectX::Colors::Transparent, DirectX::Colors::White)
{
	float colliderHalfSize = 25.0f;
	float extraOffset = 10.0f;

	m_colliders.push_back(new GameCollider(this, GameManager::CollisionTag::wall, DirectX::SimpleMath::Vector2(-m_halfSize.x - colliderHalfSize, 0.0f), DirectX::SimpleMath::Vector2(colliderHalfSize, m_halfSize.y + extraOffset) * 2.0f));
	m_colliders.push_back(new GameCollider(this, GameManager::CollisionTag::wall, DirectX::SimpleMath::Vector2(0.0f, -m_halfSize.y - colliderHalfSize), DirectX::SimpleMath::Vector2(m_halfSize.x + extraOffset, colliderHalfSize) * 2.0f));
	m_colliders.push_back(new GameCollider(this, GameManager::CollisionTag::wall, DirectX::SimpleMath::Vector2(m_halfSize.x + colliderHalfSize, 0.0f), DirectX::SimpleMath::Vector2(colliderHalfSize, m_halfSize.y + extraOffset) * 2.0f));
	m_colliders.push_back(new GameCollider(this, GameManager::CollisionTag::death, DirectX::SimpleMath::Vector2(0.0f, m_halfSize.y + colliderHalfSize), DirectX::SimpleMath::Vector2(m_halfSize.x + extraOffset, colliderHalfSize) * 2.0f));
}

void GameField::Update(float i_deltaTime, const DirectX::Keyboard::State& i_kb)
{
	GameObject::Update(i_deltaTime, i_kb);
}

void GameField::Draw(DirectX::PrimitiveBatch<GameObject::VertexType>* i_batch) const
{
	GameObject::Draw(i_batch);
}