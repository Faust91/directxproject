#pragma once
#include "GameObject.h"
#include "GameManager.h"

class GameCollider : public GameObjectBase
{
private:
	static constexpr bool COLLIDER_DRAW_DEBUG = false;

protected:
	GameManager::CollisionTag m_collisionTag;
	GameObject* m_parent;
	std::vector<GameCollider*> m_collisions;
	std::vector<GameCollider*> m_newCollisions;
	std::vector<GameCollider*> m_oldCollisions;
	std::vector<GameCollider*> m_endedCollisions;
	std::vector<GameCollider*> m_tempCollisions;
	void updateCollisions();

public:
	GameCollider();
	GameCollider(GameObject* i_parent, GameManager::CollisionTag i_collisionTag);
	GameCollider(GameObject* i_parent, GameManager::CollisionTag i_collisionTag, DirectX::SimpleMath::Vector2 i_position);
	GameCollider(GameObject* i_parent, GameManager::CollisionTag i_collisionTag, DirectX::SimpleMath::Vector2 i_position, DirectX::SimpleMath::Vector2 i_size);
	virtual void Update(float i_deltaTime, const DirectX::Keyboard::State& i_kb) override;
	virtual void Draw(DirectX::PrimitiveBatch<VertexType>* i_batch) const override;
	void setCollisionTag(GameManager::CollisionTag i_collisionTag);
	GameManager::CollisionTag getCollisionTag();
	void setParent(GameObject* i_parent);
	GameObject* getParent();
	bool collideWith(GameCollider* i_other);
	DirectX::SimpleMath::Rectangle getWorldRectangle();
};

