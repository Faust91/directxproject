#pragma once
#include "GameObject.h"
class Brick : public GameObject
{
private:
	int m_lives;
public:
	Brick(DirectX::SimpleMath::Vector2 i_position);
	Brick(DirectX::SimpleMath::Vector2 i_position, int i_lives);
	virtual void Update(float i_deltaTime, const DirectX::Keyboard::State& i_kb) override;
	virtual void Draw(DirectX::PrimitiveBatch<GameObject::VertexType>* i_batch) const override;
	void OnCollisionBegin(GameCollider* i_me, GameCollider* i_other) override;

};

