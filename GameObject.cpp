#include "pch.h"

#include "GameObject.h"
#include "GameCollider.h"

GameObject::GameObject() :
	GameObject(DirectX::SimpleMath::Vector2(0.0f, 0.0f), DirectX::SimpleMath::Vector2(0.0f, 0.0f)) {}

GameObject::GameObject(DirectX::SimpleMath::Vector2 i_position) :
	GameObject(i_position, DirectX::SimpleMath::Vector2(0.0f, 0.0f), 0.0f, DirectX::Colors::Transparent, DirectX::Colors::Transparent) {}

GameObject::GameObject(DirectX::SimpleMath::Vector2 i_position, DirectX::SimpleMath::Vector2 i_size) :
	GameObject(i_position, i_size, 0.0f, DirectX::Colors::Transparent, DirectX::Colors::Transparent) {}

GameObject::GameObject(DirectX::SimpleMath::Vector2 i_position, DirectX::SimpleMath::Vector2 i_size, DirectX::XMVECTORF32 i_color) :
	GameObject(i_position, i_size, 0.0f, i_color, DirectX::Colors::Transparent) {}

GameObject::GameObject(DirectX::SimpleMath::Vector2 i_position, DirectX::SimpleMath::Vector2 i_size, float i_padding, DirectX::XMVECTORF32 i_color, DirectX::XMVECTORF32 i_paddingColor) : GameObjectBase(i_position, i_size), m_padding(i_padding), m_color(i_color), m_paddingColor(i_paddingColor)
{
	m_velocity = DirectX::SimpleMath::Vector2(0.0f, 0.0f);
}

void GameObject::Update(float deltaTime, const DirectX::Keyboard::State& i_kb)
{
	GameObjectBase::Update(deltaTime, i_kb);

	if (m_velocity.Length() >= MIN_VELOCITY) {
		m_position += (m_velocity * deltaTime);
	}

	for (GameCollider* collider : m_colliders) {
		collider->Update(deltaTime, i_kb);
	}
}

void GameObject::Draw(DirectX::PrimitiveBatch<VertexType>* i_batch) const
{
	GameObjectBase::Draw(i_batch);

	if (m_padding > 0.0f) {
		DirectX::VertexPositionColor v1(DirectX::SimpleMath::Vector2(m_position.x - m_halfSize.x, m_position.y - m_halfSize.y), m_paddingColor);
		DirectX::VertexPositionColor v2(DirectX::SimpleMath::Vector2(m_position.x + m_halfSize.x, m_position.y - m_halfSize.y), m_paddingColor);
		DirectX::VertexPositionColor v3(DirectX::SimpleMath::Vector2(m_position.x + m_halfSize.x, m_position.y + m_halfSize.y), m_paddingColor);
		DirectX::VertexPositionColor v4(DirectX::SimpleMath::Vector2(m_position.x - m_halfSize.x, m_position.y + m_halfSize.y), m_paddingColor);
		i_batch->DrawQuad(v1, v2, v3, v4);
	}

	DirectX::VertexPositionColor v1(DirectX::SimpleMath::Vector2(m_position.x - m_halfSize.x + m_padding, m_position.y - m_halfSize.y + m_padding), m_color);
	DirectX::VertexPositionColor v2(DirectX::SimpleMath::Vector2(m_position.x + m_halfSize.x - m_padding, m_position.y - m_halfSize.y + m_padding), m_color);
	DirectX::VertexPositionColor v3(DirectX::SimpleMath::Vector2(m_position.x + m_halfSize.x - m_padding, m_position.y + m_halfSize.y - m_padding), m_color);
	DirectX::VertexPositionColor v4(DirectX::SimpleMath::Vector2(m_position.x - m_halfSize.x + m_padding, m_position.y + m_halfSize.y - m_padding), m_color);
	i_batch->DrawQuad(v1, v2, v3, v4);

	for (GameCollider* collider : m_colliders) {
		collider->Draw(i_batch);
	}
}

void GameObject::setVelocity(DirectX::SimpleMath::Vector2 i_velocity)
{
	m_velocity = i_velocity;
}

DirectX::SimpleMath::Vector2 GameObject::getVelocity()
{
	return m_velocity;
}

void GameObject::OnCollisionBegin(GameCollider* i_me, GameCollider* i_other) {}

void GameObject::OnCollisionStay(GameCollider* i_me, GameCollider* i_other) {}

void GameObject::OnCollisionEnd(GameCollider* i_me, GameCollider* i_other) {}

std::vector<GameCollider*> GameObject::getColliders()
{
	return m_colliders;
}

GameObject::~GameObject()
{
	for (GameCollider* collider : m_colliders) {
		if (collider != nullptr) {
			delete collider;
			collider = nullptr;
		}
	}
	m_colliders.clear();
}

