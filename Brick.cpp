#include "pch.h"
#include "Brick.h"
#include "GameManager.h"
#include "GameCollider.h"

Brick::Brick(DirectX::SimpleMath::Vector2 i_position) : Brick(i_position, 1) {}

Brick::Brick(DirectX::SimpleMath::Vector2 i_position, int i_lives) : GameObject(i_position, DirectX::SimpleMath::Vector2(80.0f, 30.0f), 2.0f, i_lives > 1 ? DirectX::Colors::Red : DirectX::Colors::DeepSkyBlue, DirectX::Colors::White), m_lives(i_lives)
{
	m_colliders.push_back(new GameCollider(this, GameManager::CollisionTag::brick, DirectX::SimpleMath::Vector2(0.0f, 0.0f), m_halfSize * 2));
}

void Brick::Update(float i_deltaTime, const DirectX::Keyboard::State& i_kb)
{
	GameObject::Update(i_deltaTime, i_kb);
}

void Brick::Draw(DirectX::PrimitiveBatch<GameObject::VertexType>* i_batch) const
{
	GameObject::Draw(i_batch);
}

void Brick::OnCollisionBegin(GameCollider* i_me, GameCollider* i_other)
{
	m_lives--;
	if (m_lives <= 0) {
		// elimino i collider
		for (GameCollider* collider : m_colliders) {
			if (collider != nullptr) {
				delete collider;
				collider = nullptr;
			}
		}
		m_colliders.clear();
		// mi rendo trasparente
		m_color = DirectX::Colors::Transparent;
		m_paddingColor = DirectX::Colors::Transparent;
		// decremento il numero di mattoni nel manager
		GameManager::instance().decrementNumBlocs();
	}
	else if (m_lives == 1)
	{
		m_color = DirectX::Colors::DeepSkyBlue;
	}
	else {
		m_color = DirectX::Colors::Red;
	}
}
