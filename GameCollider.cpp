#include "pch.h"
#include "GameCollider.h"

void GameCollider::updateCollisions()
{
	if (!m_newCollisions.empty()) {
		m_newCollisions.clear();
	}

	if (!m_oldCollisions.empty()) {
		m_oldCollisions.clear();
	}

	if (!m_endedCollisions.empty()) {
		m_endedCollisions.clear();
	}

	if (!m_tempCollisions.empty()) {
		m_tempCollisions.clear();
	}

	for (GameCollider* collider : GameManager::instance().getGameColliders(m_collisionTag)) {
		if (collider->collideWith(this)) {
			m_tempCollisions.push_back(collider);
			bool found = false;
			for (GameCollider* oldCollision : m_collisions) {
				if (oldCollision == collider) {
					found = true;
					break;
				}
			}
			if (found) {
				m_oldCollisions.push_back(collider);
			}
			else {
				m_newCollisions.push_back(collider);
			}
		}
	}

	for (GameCollider* prevCollider : m_collisions) {
		bool found = false;
		for (GameCollider* oldCollider : m_oldCollisions) {
			if (prevCollider == oldCollider) {
				found = true;
				break;
			}
		}
		if (!found) {
			m_endedCollisions.push_back(prevCollider);
		}
	}

	if (!m_collisions.empty()) {
		m_collisions.clear();
	}

	for (GameCollider* collision : m_tempCollisions) {
		m_collisions.push_back(collision);
	}
}

GameCollider::GameCollider() :
	GameCollider(nullptr, GameManager::wall, DirectX::SimpleMath::Vector2(0.0f, 0.0f), DirectX::SimpleMath::Vector2(0.0f, 0.0f)) {}

GameCollider::GameCollider(GameObject* i_parent, GameManager::CollisionTag i_collisionTag) :
	GameCollider(i_parent, i_collisionTag, DirectX::SimpleMath::Vector2(0.0f, 0.0f), DirectX::SimpleMath::Vector2(0.0f, 0.0f)) {}

GameCollider::GameCollider(GameObject* i_parent, GameManager::CollisionTag i_collisionTag, DirectX::SimpleMath::Vector2 i_position) :
	GameCollider(i_parent, i_collisionTag, i_position, DirectX::SimpleMath::Vector2(0.0f, 0.0f)) {}

GameCollider::GameCollider(GameObject* i_parent, GameManager::CollisionTag i_collisionTag, DirectX::SimpleMath::Vector2 i_position, DirectX::SimpleMath::Vector2 i_size) :
	GameObjectBase(i_position, i_size), m_parent(i_parent), m_collisionTag(i_collisionTag) {
	m_collisions = std::vector<GameCollider*>();
	m_newCollisions = std::vector<GameCollider*>();
	m_oldCollisions = std::vector<GameCollider*>();
	m_endedCollisions = std::vector<GameCollider*>();
	m_tempCollisions = std::vector<GameCollider*>();
}

void GameCollider::Update(float i_deltaTime, const DirectX::Keyboard::State& i_kb)
{
	GameObjectBase::Update(i_deltaTime, i_kb);

	updateCollisions();

	if (!m_newCollisions.empty()) {
		for (GameCollider* collider : m_newCollisions) {
			m_parent->OnCollisionBegin(this, collider);
		}
	}

	if (!m_oldCollisions.empty()) {
		for (GameCollider* collider : m_oldCollisions) {
			m_parent->OnCollisionStay(this, collider);
		}
	}

	if (!m_endedCollisions.empty()) {
		for (GameCollider* collider : m_endedCollisions) {
			m_parent->OnCollisionEnd(this, collider);
		}
	}
}

void GameCollider::Draw(DirectX::PrimitiveBatch<VertexType>* i_batch) const
{
	GameObjectBase::Draw(i_batch);

	if (COLLIDER_DRAW_DEBUG) {
		float padding = 1.0f;
		DirectX::XMVECTORF32 color = DirectX::Colors::CornflowerBlue;

		DirectX::VertexPositionColor v1(DirectX::SimpleMath::Vector2(m_parent->getPosition().x + m_position.x - m_halfSize.x + padding, m_parent->getPosition().y + m_position.y - m_halfSize.y + padding), color);
		DirectX::VertexPositionColor v2(DirectX::SimpleMath::Vector2(m_parent->getPosition().x + m_position.x + m_halfSize.x - padding, m_parent->getPosition().y + m_position.y - m_halfSize.y + padding), color);
		DirectX::VertexPositionColor v3(DirectX::SimpleMath::Vector2(m_parent->getPosition().x + m_position.x + m_halfSize.x - padding, m_parent->getPosition().y + m_position.y + m_halfSize.y - padding), color);
		DirectX::VertexPositionColor v4(DirectX::SimpleMath::Vector2(m_parent->getPosition().x + m_position.x - m_halfSize.x + padding, m_parent->getPosition().y + m_position.y + m_halfSize.y - padding), color);

		i_batch->DrawQuad(v1, v2, v3, v4);
	}
}

void GameCollider::setCollisionTag(GameManager::CollisionTag i_collisionTag)
{
	m_collisionTag = i_collisionTag;
}

GameManager::CollisionTag GameCollider::getCollisionTag()
{
	return m_collisionTag;
}

void GameCollider::setParent(GameObject* i_parent)
{
	m_parent = i_parent;
}

GameObject* GameCollider::getParent()
{
	return m_parent;
}

bool GameCollider::collideWith(GameCollider* i_other)
{
	return !DirectX::SimpleMath::Rectangle::Intersect(this->getWorldRectangle(), i_other->getWorldRectangle()).IsEmpty();
}

DirectX::SimpleMath::Rectangle GameCollider::getWorldRectangle()
{
	return DirectX::SimpleMath::Rectangle(m_parent->getPosition().x + m_position.x - m_halfSize.x, m_parent->getPosition().y + m_position.y - m_halfSize.y, m_halfSize.x * 2.0f, m_halfSize.y * 2.0f);
}
