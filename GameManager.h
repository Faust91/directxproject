#pragma once

class GameObject;
class GameCollider;

class GameManager
{
private:
	GameManager();
	std::vector<GameObject*> m_gameObjects;
	//{     , WALL,SCORE,BRICK, BALL,DEATH}
	//{ WALL,     ,     ,     ,     ,     }
	//{SCORE,     ,     ,     ,     ,     }
	//{BRICK,     ,     ,     ,     ,     }
	//{ BALL,     ,     ,     ,     ,     }
	//{DEATH,     ,     ,     ,     ,     }
	bool m_collisionMatrix[5][5] = {
		{false, true,false, true,false},
		{ true,false,false, true,false},
		{false,false,false, true,false},
		{ true, true, true,false, true},
		{false,false,false, true,false}
	};

public:
	enum CollisionTag {
		wall, score, brick, ball, death
	};

private:
	int m_numBlocks;
	bool canCollide(CollisionTag i_tag1, CollisionTag i_tag2);
	DirectX::SimpleMath::Vector2 m_center;

public:
	static GameManager& instance();
	void addGameObject(GameObject* i_gameObjects);
	std::vector<GameObject*> getGameObjects();
	std::vector<GameCollider*> getGameColliders(CollisionTag i_tag);
	void setNumBlocs(int i_numBlocks);
	int getNumBlocs();
	void incrementNumBlocs();
	void decrementNumBlocs();
	void gameOver(bool i_restart);
	void setCenter(DirectX::SimpleMath::Vector2 i_center);

};