#pragma once
#include "GameObject.h"

class GameCollider;

class Ball : public GameObject
{
private:
	bool yReverted = false;
	bool xReverted = false;

public:
	Ball(DirectX::SimpleMath::Vector2 i_position);
	virtual void Update(float i_deltaTime, const DirectX::Keyboard::State& i_kb) override;
	virtual void Draw(DirectX::PrimitiveBatch<GameObject::VertexType>* i_batch) const override;
	void OnCollisionBegin(GameCollider* i_me, GameCollider* i_other) override;
	void OnCollisionEnd(GameCollider* i_me, GameCollider* i_other) override;

};

