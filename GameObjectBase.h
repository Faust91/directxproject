#pragma once

class GameObjectBase
{
public:
	using VertexType = DirectX::VertexPositionColor;

protected:
	DirectX::SimpleMath::Vector2 m_position;
	DirectX::SimpleMath::Vector2 m_halfSize;

public:
	GameObjectBase();
	GameObjectBase(DirectX::SimpleMath::Vector2 i_position);
	GameObjectBase(DirectX::SimpleMath::Vector2 i_position, DirectX::SimpleMath::Vector2 i_size);
	virtual void Update(float i_deltaTime, const DirectX::Keyboard::State& i_kb);
	virtual void Draw(DirectX::PrimitiveBatch<VertexType>* i_batch) const;
	void setPosition(DirectX::SimpleMath::Vector2 i_position);
	DirectX::SimpleMath::Vector2 getPosition();
	DirectX::SimpleMath::Vector2 getSize();
	DirectX::SimpleMath::Vector2 getHalfSize();

};

